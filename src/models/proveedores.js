const Sequelize = require('sequelize');
module.exports  = function (sequelize, DataTypes) {
  return sequelize.define('proveedores', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    codigo: {
      type: DataTypes.STRING(45),
      allowNull: false,
      unique: "Unique_Codigo"
    },
    razonsocial: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    direccion: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(250),
      allowNull: false
    },
  }, {
    sequelize,
    tableName: 'proveedores',
    timestamps: false,
  })
}
