const DataTypes = require("sequelize").DataTypes;
const _productos = require("./productos");
const _proveedores = require("./proveedores");


function initModels(sequelize) {
  let productos = _productos(sequelize, DataTypes);
  let proveedores = _proveedores(sequelize, DataTypes);


  return {
    productos,
    proveedores
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
