const {Router} = require('express');
const {models} = require('../db/index');
const router = Router();

//Raíz
router.get("/", async(req,res) =>{
    try{
        let LstProductos = await models.productos.findAll();
        res.json(LstProductos);
    }catch(error){
        console.log(error);
    }
});

router.get("/:id", async(req,res) =>{
    try{
        const {id} = req.params;
        let producto = await models.productos.findByPk(id);
        res.json(producto);
    }catch(error){
        console.log(error);
    }
});


router.post("/", async(req,res) =>{
    const { body } = req;
    console.log(body);
    res.json(body);
});

router.put("/:id", async(req,res) =>{
    try{
        const {id} = req.params;
        const {body} = req;
        const update = await models.productos.update(body,{
            where:{id: id}
        });
        let product = await models.productos.findByPk(id);
    }catch(error){
        console.log(error);
    }
});

router.delete("/:id", async(req,res) =>{
    try{
        const {id}=req.params;
        await models.productos.destroy({
            where:{
                id:ID
            }
        });
    }catch(error){
        console.log(error);
    }
})
module.exports = router;
