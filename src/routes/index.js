const { Router } = require("express");
const router = Router();

//Root
router.get('/', (req, res) => {
    res.json({
        "Titulo": "API Tienda"
    });
});

//exportar el modulo
module.exports = router;