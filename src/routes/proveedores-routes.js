const {Router} = require('express');
const {models} = require('../db/index');
const router = Router();

//Raíz
router.get("/", async(req,res) =>{
  try{
    let lstProveedores = await models.proveedores.findAll();
    res.json(lstProveedores);
  }catch(error){
    console.log(error);
  }
});

router.get("/:id", async(req,res) =>{
  try{
    const {id} = req.params;
    let proveedores = await models.proveedores.findByPk(id);
    res.json(proveedores);
  }catch(error){
    console.log(error);
  }
});

router.post("/", async(req,res) =>{
  const { body } = req;
  console.log(body);
  const proveedor = await models.proveedores.create(body);
  res.json(proveedor);
});

router.put("/:id", async(req,res) =>{
  try{
    const {id} = req.params;
    const {body} = req;
    const update = await models.proveedores.update(body,{
      where:{id: id}
    });
    l

  }catch(error){
    console.log(error);
  }
});

router.delete("/:id", async(req,res) =>{
  try{
    const {id}=req.params;
    await models.proveedores.destroy({
      where:{
        id:id
      }

    });
    res.json({exito:true});
  }catch(error){
    console.log(error);
  }
})
module.exports = router;
